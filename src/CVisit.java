import java.util.Date;

public class CVisit {
    private Customer customer;
    private Date date = new Date();
    private double serviceExpense = 0;
    private double productExpense = 0;

    public CVisit(Customer customer, Date date, double serviceExpense, double productExpense) {
        this.customer = customer;
        this.date = date;
        this.serviceExpense = serviceExpense;
        this.productExpense = productExpense;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getServiceExpense() {
        return serviceExpense;
    }

    public void setServiceExpense(double serviceExpense) {
        this.serviceExpense = serviceExpense;
    }

    public double getProductExpense() {
        return productExpense;
    }

    public void setProductExpense(double productExpense) {
        this.productExpense = productExpense;
    }

    ////////////////////////////
    public double getTotalExpense() {
        double totalExpense = this.serviceExpense + this.productExpense;
        return totalExpense;
    }
    ////////////////////////

    ///////////////////////////
    @Override
    public String toString() {
        return "CVisit [customer=" + customer
                + ", date=" + date
                + ", productExpense=" + productExpense

                + ", serviceExpense=" + serviceExpense
                + ", totalExpense=" + this.getTotalExpense()

                + "]";
    }

    public CVisit(Customer customer, double serviceExpense, double productExpense) {
        this.customer = customer;
        this.serviceExpense = serviceExpense;
        this.productExpense = productExpense;
    }

}
